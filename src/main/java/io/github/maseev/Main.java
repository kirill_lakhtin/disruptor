package io.github.maseev;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.github.maseev.disruptor.consumer.ReadStatisticEventHandler;
import io.github.maseev.disruptor.consumer.WriteStatisticsEventHandler;
import io.github.maseev.disruptor.data.StatisticEvent;

public class Main {

  public static final int NUM_OF_MESSAGES = 5_000_000;
  private static final int BUFFER_SIZE = 1024;
  public static final int NUM_OF_WORKERS = 4;

  public static void main(String[] args) {
    Disruptor<StatisticEvent> writeDisruptor = getWriteDisruptor();

    Executor executor = Executors.newFixedThreadPool(NUM_OF_WORKERS);
    // Specify the size of the ring buffer, must be power of 2.
    // Construct the Disruptor
    Disruptor<StatisticEvent> disruptor = new Disruptor<>(StatisticEvent::new, BUFFER_SIZE, executor);
    // Connect the handler
    ReadStatisticEventHandler[] handlers = new ReadStatisticEventHandler[NUM_OF_WORKERS];
    for (int i = 0; i < NUM_OF_WORKERS; i++) {
      handlers[i] = new ReadStatisticEventHandler(i, writeDisruptor);
    }
    disruptor.handleEventsWith(handlers);
    // Start the Disruptor, starts all threads running
    writeDisruptor.start();
    disruptor.start();
    // Get the ring buffer from the Disruptor to be used for publishing.
    RingBuffer<StatisticEvent> ringBuffer = disruptor.getRingBuffer();
    int index = 0;
    for (long l = 0; l < NUM_OF_MESSAGES; l++) {
      final int num = index++;
      long onReadPublishTime = System.nanoTime();
      ringBuffer.publishEvent((StatisticEvent e, long s) -> {
        e.setOnReadPublishTime(onReadPublishTime);
        e.setNumber(num);
      });
      if (index == NUM_OF_WORKERS) {
        index = 0;
      }
    }

    System.out.println("Read workers statistics");
    for (int i = 0; i < NUM_OF_WORKERS; i++) {
      System.out.println("Worker " + i + " processed " + handlers[i].getMessageProcessed() + " messages.");
    }
  }

  private static Disruptor<StatisticEvent> getWriteDisruptor() {
    Executor executor = Executors.newFixedThreadPool(1);
    // Specify the size of the ring buffer, must be power of 2.
    // Construct the Disruptor
    Disruptor<StatisticEvent> disruptor = new Disruptor<>(StatisticEvent::new, BUFFER_SIZE, executor);
    // Connect the handler
    WriteStatisticsEventHandler handler1 = new WriteStatisticsEventHandler();
    disruptor.handleEventsWith(handler1);
    return disruptor;
  }
}
