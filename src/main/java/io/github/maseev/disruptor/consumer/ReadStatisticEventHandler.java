package io.github.maseev.disruptor.consumer;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

import io.github.maseev.disruptor.data.StatisticEvent;

public class ReadStatisticEventHandler implements EventHandler<StatisticEvent> {
  private final Disruptor<StatisticEvent> disruptor;
  private final int index;
  private int messageProcessed = 0;

  public ReadStatisticEventHandler(int index, Disruptor<StatisticEvent> writeDisruptor) {
    this.index = index;
    this.disruptor = writeDisruptor;
  }

  @Override
  public void onEvent(StatisticEvent event, long a, boolean b) throws Exception {
    if (event.getNumber() == index) {
      processEvent(event);
    }
  }

  private void processEvent(StatisticEvent event) {
    messageProcessed++;
    event.setOnReadEventTime(System.nanoTime());
    RingBuffer<StatisticEvent> ringBuffer = disruptor.getRingBuffer();
    long onWritePublishTime = System.nanoTime();
    ringBuffer.publishEvent((e, s) -> {
      e.setOnReadPublishTime(event.getOnReadPublishTime());
      e.setOnReadEventTime(event.getOnReadEventTime());
      e.setOnWritePublishTime(onWritePublishTime);
    });
  }

  public int getMessageProcessed() {
    return messageProcessed;
  }
}
