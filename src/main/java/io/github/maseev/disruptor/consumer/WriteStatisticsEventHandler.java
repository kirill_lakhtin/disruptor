package io.github.maseev.disruptor.consumer;

import com.lmax.disruptor.EventHandler;

import it.unimi.dsi.fastutil.longs.LongArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

import io.github.maseev.Main;
import io.github.maseev.disruptor.data.StatisticEvent;

/**
 * @author Kirill Lakhtin.
 */
public class WriteStatisticsEventHandler implements EventHandler<StatisticEvent> {

  private static final int NUMBER_OF_FIELDS_PLUS_DELIMITER = 5;

  private static final int DELIMITER = -1;

  private static final int EXPECTED_SIZE = Main.NUM_OF_MESSAGES * NUMBER_OF_FIELDS_PLUS_DELIMITER;

  private LongArrayList statistic =
    new LongArrayList(EXPECTED_SIZE);

  @Override
  public void onEvent(StatisticEvent event, long sequence, boolean endOfBatch) throws Exception {
    event.setOnWriteReadTime(System.nanoTime());

    statistic.add(event.getOnReadPublishTime());
    statistic.add(event.getOnReadEventTime());
    statistic.add(event.getOnWritePublishTime());
    statistic.add(event.getOnWriteReadTime());
    statistic.add(DELIMITER);

    if (statistic.size() == EXPECTED_SIZE) {
      List<Long> readList = new ArrayList<>(Main.NUM_OF_MESSAGES);
      List<Long> writeList = new ArrayList<>(Main.NUM_OF_MESSAGES);
      List<Long> resultList = new ArrayList<>(Main.NUM_OF_MESSAGES);
      for (int i = 0, size = statistic.size(); i < size; i += 5) {
        final long onReadPublishTime = statistic.getLong(i);
        final long onReadEventTime = statistic.getLong(i + 1);
        final long onWritePublishTime = statistic.getLong(i + 2);
        final long onWriteEventTime = statistic.getLong(i + 3);
        readList.add(onReadEventTime - onReadPublishTime);
        writeList.add(onWriteEventTime - onWritePublishTime);
        resultList.add(onWriteEventTime - onReadPublishTime);
      }
      System.out.println("Write worker statistics");
      OptionalDouble readAverage = readList.stream().mapToLong(n -> n).average();
      System.out.println("First ring stats : processed: " + readList.size() + " Avg: " + getTimeLog((long) readAverage.getAsDouble()));

      OptionalDouble writeAverage = writeList.stream().mapToLong(n -> n).average();
      System.out.println("Second ring stats : processed: " + writeList.size() + " Avg: " + getTimeLog((long) writeAverage.getAsDouble()));

      OptionalDouble processingAverage = resultList.stream().mapToLong(n -> n).average();
      System.out.println("Total processing stats : processed: " + writeList.size() + " Avg: " + getTimeLog((long) processingAverage.getAsDouble()));


      statistic.clear();
    }
  }

  private static String getTimeLog(long value) {
    if (value > 1000_000_000) {
      return Long.toString(value / 1000_000_000) + " s";
    } else if (value > 1000_000) {
      return Long.toString(value / 1000_000) + " ms";
    } else if (value > 1000) {
      return Long.toString(value / 1000) + " µs";
    }
    return Long.toString(value) + " ns";
  }
}
