package io.github.maseev.disruptor.data.factory;

import com.lmax.disruptor.EventFactory;

import io.github.maseev.disruptor.data.StatisticEvent;

public class StatisticEventFactory implements EventFactory<StatisticEvent> {

  public StatisticEvent newInstance() {
    return new StatisticEvent();
  }
}
