package io.github.maseev.disruptor.data;

public class StatisticEvent {

  private long onReadPublishTime;
  private long onReadEventTime;
  private long onWritePublishTime;
  private long onWriteReadTime;

  private long number;

  public long getNumber() {
    return number;
  }

  public void setNumber(long number) {
    this.number = number;
  }

  public long getOnReadPublishTime() {
    return onReadPublishTime;
  }

  public void setOnReadPublishTime(long onReadPublishTime) {
    this.onReadPublishTime = onReadPublishTime;
  }

  public long getOnReadEventTime() {
    return onReadEventTime;
  }

  public void setOnReadEventTime(long onReadEventTime) {
    this.onReadEventTime = onReadEventTime;
  }

  public long getOnWriteReadTime() {
    return onWriteReadTime;
  }

  public void setOnWriteReadTime(long onWriteReadTime) {
    this.onWriteReadTime = onWriteReadTime;
  }

  public long getOnWritePublishTime() {
    return onWritePublishTime;
  }

  public void setOnWritePublishTime(long onWritePublishTime) {
    this.onWritePublishTime = onWritePublishTime;
  }
}
